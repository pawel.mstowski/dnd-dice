from dnd_dice.cli import main
import sys

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
