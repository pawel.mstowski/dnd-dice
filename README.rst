========
DnD Dice
========


.. image:: https://img.shields.io/pypi/v/dnd_dice.svg
        :target: https://pypi.python.org/pypi/dnd_dice

.. image:: https://img.shields.io/travis/mustafmst/dnd_dice.svg
        :target: https://travis-ci.com/mustafmst/dnd_dice

.. image:: https://readthedocs.org/projects/dnd-dice/badge/?version=latest
        :target: https://dnd-dice.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Shell simulating Dungeons and Dragons dice rolls


* Free software: MIT license
* Documentation: https://dnd-dice.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
